#include <stdio.h>
#include <unistd.h>
#include <sys/ioctl.h>

static int oneSecond = 1000000; //is equal to 1 sec with usleep
static char chaine[]= "00:00:00"; //maycountOnee later

static int firstHalf = 50;
static int secondHalf = 50;
static int full = 100;

static int isAscending = 1;
static int isAlsoAscending = 1;
static int i1 = 0;
static int countOne = 0;
static int i2 = 0;
static int countTwo = 100;


static int ms = 0;
static int se = 0;
static int mi = 0;


void space(){
  if(isAscending){
    for(i1=0; i1 <=countOne; i1++){
      printf(" ");
    }
    countOne++;
  }else {
    for(i1=0; i1 <=countOne; i1++){
      printf(" ");
    }
    countOne--;
  }
  if(countOne>=full) isAscending=0;
  if(countOne<=0) isAscending=1;
}

void ecaps(){
  if(!isAlsoAscending){
    for(i2=0; i2 <=countTwo; i2++){
      printf(" ");
    }
    countTwo++;
  }else {
    for(i2=0; i2 <=countTwo; i2++){
      printf(" ");
    }
    countTwo--;
  }
  if(countTwo>=full) isAlsoAscending=1;
  if(countTwo<=0) isAlsoAscending=0;
}

void afficheTemps(int h, int m, int s){
  if(h>=0 && h<=9){
    printf("0%d:",h);
  }else{
    printf("%d:",h);
  }

  if(m>=0 && m<=9){
    printf("0%d:",m);
  }else{
    printf("%d:",m);
  }

  if(s>=0 && s<=9){
    printf("0%d",s);
  }else{
    printf("%d",s);
  }
}

int main(){
  struct winsize w;
  ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
  //printf ("lines %d\n", w.ws_row);
	printf ("columns %d\n", w.ws_col);

  int loop = 1;
  full = w.ws_col - 8 - 1 - 8 - 1 -8;
  //full = 100;
  countTwo = full;

  while(loop){
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

    if (full != w.ws_col - 8 - 1 - 8 - 1 -8){
      full = w.ws_col - 8 - 1 - 8 - 1 -8;
      countTwo = full - countOne;
      countOne = full - countTwo;
    }


    afficheTemps(mi,se,ms);
    space();
    afficheTemps(mi,se,ms);
    ecaps();
    afficheTemps(mi,se,ms);
    printf("\n");

    ms++;
    if(ms==100){
      se++;
      ms=0;
    }
    if(se==60){
      mi++;
      se=0;
    }
    if(mi==60){
      //later
    }
    // instead of 0.01*oneSecond --> oneSecond/full;
    usleep(oneSecond/full);
  }

  return 0;
}
