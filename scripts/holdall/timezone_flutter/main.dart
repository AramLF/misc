import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Time Display App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: TimeDisplayScreen(),
    );
  }
}

class TimeDisplayScreen extends StatefulWidget {
  @override
  _TimeDisplayScreenState createState() => _TimeDisplayScreenState();
}

class _TimeDisplayScreenState extends State<TimeDisplayScreen> {
  late Timer _timer;
  late DateTime parisTime;
  late DateTime coloradoTime;
  late DateTime californiaTime;

  @override
  void initState() {
    super.initState();
    _updateTimes();
    _timer = Timer.periodic(Duration(seconds: 1), _updateTimes);
  }

  void _updateTimes([Timer? timer]) {
    final now = DateTime.now();
    parisTime = now.toUtc().add(Duration(hours: 2));
    coloradoTime = now.toUtc().add(Duration(hours: -6));
    californiaTime = now.toUtc().add(Duration(hours: -7));
    setState(() {});
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF242424),
      appBar: AppBar(
        backgroundColor: Color(0xFF242424),
        title: Text('Time Display App',style: TextStyle(color: Colors.red,)),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _buildTimeRow('Local Time', DateTime.now()),
            _buildTimeRow('Paris Time', parisTime),
            _buildTimeRow('Colorado Time', coloradoTime),
            _buildTimeRow('California Time', californiaTime),
          ],
        ),
      ),
    );
  }

  Widget _buildTimeRow(String label, DateTime time) {
    return Column(
      children: [
        Text(
          label,
          style: TextStyle(color: Colors.white,fontSize: 20),
        ),
        Text(
          DateFormat('HH:mm:ss').format(time),
          style: TextStyle(color: Colors.white,fontFamily: 'monospace',fontSize: 48),
        ),
      ],
    );
  }
}
