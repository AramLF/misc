#include <getopt.h>
#include <stdio.h>
#include "echo.h"

void printHelpMessage(void){
	printf("myecho [-n] [msg...]\n");
	printf("myecho {--help, --version}\n");
}

int main(int argc, char **argv)
{
	int newLine = 1;
	int loopIsOver = 0;

	const struct option options[] = {
		{"help", no_argument, NULL, 'h'},
		{"version", no_argument, NULL, 'v'},
		{0, 0, 0, 0}
	};

	while(!loopIsOver){
		//https :// www.gnu.org/software/libc/manual/html_node/Getopt-Long-Options-html#Getopt-Long-Options
		int c = getopt_long(argc, argv, "nhv", options, NULL);
		switch(c){
			case 'n':
				newLine = 0;
				break;
			case 'h':
				printHelpMessage();
				return 0;
			case 'v':
				printf("myecho v0.1\n");
				return 0;
			case -1:
				loopIsOver = 1;
				break;
			default:
				printHelpMessage();
				return 1;
		}
	}

	//echo(argc,argv);
	//printf("\n");

	echo(argc-optind, argv+optind);
	if (newLine) printf("\n");
	return 0;
}