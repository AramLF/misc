import 'dart:io';
import 'dart:async';

const int PROGRESS_BAR_WIDTH = 40;
const int TOTAL_ITERATIONS = 100;

void displayProgressBar(int progress) {
  int barWidth = (progress * PROGRESS_BAR_WIDTH) ~/ TOTAL_ITERATIONS;
  stdout.write('[');
  for (int i = 0; i < PROGRESS_BAR_WIDTH; i++) {
    if (i < barWidth) {
      stdout.write('=');
    } else {
      stdout.write(' ');
    }
  }
  stdout.write('] ${((progress * 100) ~/ TOTAL_ITERATIONS)}%\r');
  stdout.flush();
}

Future<void> main() async {
  for (int i = 0; i <= TOTAL_ITERATIONS; i++) {
    displayProgressBar(i);
    await Future.delayed(Duration(milliseconds: 100)); // Add a small delay to simulate progress
  }

  stdout.write('\nProgress completed!\n');
}
