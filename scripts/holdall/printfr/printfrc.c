#include <stdio.h>
#include <unistd.h>

#define PROGRESS_BAR_WIDTH 40
#define TOTAL_ITERATIONS 100

void displayProgressBar(int progress) {
    int barWidth = (progress * PROGRESS_BAR_WIDTH) / TOTAL_ITERATIONS;
    printf("[");
    for (int i = 0; i < PROGRESS_BAR_WIDTH; i++) {
        if (i < barWidth) {
            printf("=");
        } else {
            printf(" ");
        }
    }
    printf("] %d%%\r", (progress * 100) / TOTAL_ITERATIONS);
    fflush(stdout);
}

int main() {
    for (int i = 0; i <= TOTAL_ITERATIONS; i++) {
        displayProgressBar(i);
        usleep(100000); // Add a small delay to simulate progress
    }

    printf("\nProgress completed!\n");
    return 0;
}