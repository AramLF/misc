use std::thread;
use std::time::Duration;
use std::io::Write; // Add this line to bring the Write trait into scope

const PROGRESS_BAR_WIDTH: usize = 40;
const TOTAL_ITERATIONS: usize = 100;

fn display_progress_bar(progress: usize) {
    let bar_width = (progress * PROGRESS_BAR_WIDTH) / TOTAL_ITERATIONS;
    print!("[");
    for i in 0..PROGRESS_BAR_WIDTH {
        if i < bar_width {
            print!("=");
        } else {
            print!(" ");
        }
    }
    print!("] {}%\r", (progress * 100) / TOTAL_ITERATIONS);
    std::io::stdout().flush().unwrap();
}

fn main() {
    for i in 0..=TOTAL_ITERATIONS {
        display_progress_bar(i);
        thread::sleep(Duration::from_millis(100)); // Add a small delay to simulate progress
    }

    println!("\nProgress completed!");
}