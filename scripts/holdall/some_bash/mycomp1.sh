#!/bin/bash

#echo "Le 1er paramètre est : $1"
#echo "script à compiler :"
#read var

#ou juste gcc $1 avec a.out

echo "exécution de : gcc -c $1.c -o $1.o"
gcc -c $1.c -o $1.o
echo "exécution de : gcc $1.o -o $1"
gcc $1.o -o $1
echo "exécution de : ./$1"
echo
echo "**************************************"
echo
./$1
echo
echo "**************************************"
echo

if [ $2 = "del" ];
then
	echo "suppression de $1.o"
	rm $1.o
	echo "suppression de $1"
	rm $1	
fi
