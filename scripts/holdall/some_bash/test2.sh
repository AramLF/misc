#!/bin/bash
n1=-1
n2=-1

until [[ $n1 -ge 0 && $n2 -ge 0 ]]; do
	echo -n "number␣1␣=␣" && read n1
	echo -n "number␣2␣=␣" && read n2
done

let  mean=($n1+$n2)/2

if [ $mean -ge 5 ]; then
	echo "mean␣=␣$mean"
else
	echo "Mean␣is␣less␣than␣5"  >&2
	exit 1
fi