#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <signal.h>
#include <time.h>
#include <sys/ioctl.h>

static int s = 1000000; //is equal to 1 sec with usleep

int main(){
	pid_t pid;
	pid = fork();

	printf("Hi !\n");
	//add many usefull fct
	int a = 1;
	//printf("%d \n",a);
	//%c %s etc...

	//usleep(s*0.01);

	switch(pid) {
		case -1:
			perror("fork");
			//return EXIT_FAILURE;
			break;
		case 0:
			printf("I am the child process. My id is %d. My parent’s id is %d.\n", getpid(), getppid());
			/*while(1){
			printf("test"); //broken af
			sleep(1);
			//}
			break;*/
			return 0;
		default:
			usleep(s*0.01);
			printf("Hello! I am the parent process. My process id is %d, my parent’s id is %d and I am the parentof process %d\n", getpid(), getppid(), pid);
		break;
	}

	printf("\n");

	//localtime example
	time_t rawtime;
	struct tm * timeinfo;

	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	printf ( "Current local time and date: %s", asctime (timeinfo) );

	printf("\n");

	//Current lines & columns of the terminal
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

	printf ("lines %d\n", w.ws_row);
	printf ("columns %d\n", w.ws_col);

	int coled= w.ws_col / 2;
	int coledMod = w.ws_col / 2 + w.ws_col % 2;

	printf("Half column : %d\n", coled);
	printf("Other half : %d\n", coledMod);

	printf("\n");
	//txt file

	int fileTxt = open("helpCText.txt", O_RDONLY);
	//printf("%d \n",fileTxt);

	if (fileTxt != -1){

		char buffer[64];
		int resu = read(fileTxt, buffer, 64);
		printf("buffer: \n\n%s\n",buffer);
		//printf("resu: %d\n",resu);

		/*if (resu != -1){
		num1=buffer[0];
		printf("num1: %d\n",buffer[0]);
		num2=buffer[1];
		printf("num2: %d\n",buffer[1]);
		printf ("average=%f\n", (float)(num1+num2)/2);
		//}*/
	}

	char test[] = "yo\no\ntest";

	printf("%s",test);

	printf("\n");

	return 0;
}
