#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <signal.h>
#include <time.h>
#include <sys/ioctl.h>

static int oneSecond = 1000000; //is equal to 1 sec with usleep

int main(){
	printf("\n");

	//localtime example
	time_t rawtime;
	struct tm * timeinfo;

	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	printf ( "Current local time and date: %s", asctime (timeinfo) );

	printf("\n");

	//Current lines & columns of the terminal
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

	printf ("lines %d\n", w.ws_row);
	printf ("columns %d\n", w.ws_col);

	int coled= w.ws_col / 2;
	int coledMod = w.ws_col / 2 + w.ws_col % 2;

	printf("Half column : %d\n", coled);
	printf("Other half : %d\n", coledMod);
	printf("\n");

	while(1){
		for(int i = 0; i < w.ws_col-1; i++){
			ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
			printf(" ");

		}
		printf("y\n");
		usleep(0.01*oneSecond);

	}

	return 0;
}
