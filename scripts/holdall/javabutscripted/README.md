# javabutscripted

Experminmenting with Tampermonkey and Violentmonkey using this README.md    
You can try to load the javascript by using a userscript manager  

## Have an id
```
Does this have an ID?
```
```
Does this have an ID too? (the same one it seems)
```

### Doing tables in a MD is weird

| On the left  | Centered          | On the right |
| :--------------- |:---------------:| -----:|
| On the left  |   this text        |  On the right |
| On the left  | is             |   On the right |
| On the left  | centered          |    On the right |

If the javascript is running, you should see colors in this table.  
You can try to change the html elements at the center by "g", "y" or "r"  
For example try to replace "this text" by "r"  
