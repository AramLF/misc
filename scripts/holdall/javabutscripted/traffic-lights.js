// ==UserScript==
// @name         traffic-lights
// @namespace    http://tampermonkey.net/
// @version      2024-10-16
// @description  Traffic lights colors in the table elements
// @author       Aliarame
// @match        https://gitlab.com/Aliarame/misc/-/tree/main/scripts/holdall/javabutscripted*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=gitlab.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    console.log("traffic-lights is running from a userscript manager");

    // Lists to match the traffic lights colors
    var theList = ["this text","is","centered"];
    var greenList = [theList[0],"green","g"];
    var yellowList = [theList[1],"yellow","y"];
    var redList = [theList[2],"red","r"];

    var els = document.querySelectorAll('td');

    // Function to change the colors
    function changeColors() {
        // Retrieve the needed elements
        els = document.querySelectorAll('td');

        for(var i = 0; i < els.length; i++) {
            var elsText = els[i].textContent.trim();

            // Change the background color, if the text of the element match a string from the lists
            if (greenList.includes(elsText)){
                els[i].closest('tr').style.backgroundColor='green';
            } else if (yellowList.includes(elsText)){
                els[i].closest('tr').style.backgroundColor='yellow';
            } else if (redList.includes(elsText)){
                els[i].closest('tr').style.backgroundColor='red';
            }

            // Uncomment the else below to reset style attributes
            /*else {
                els[i].closest('tr').style.backgroundColor='';
            }*/
        }

        // Uncomment the line below to check if the function is still running
        //console.log("traffic-lights is still running");
    }

    // Run the function every second (1000ms)
    setInterval(changeColors, 1000);

})();
